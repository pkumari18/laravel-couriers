<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('couriers', 'CourierController');
Route::get('list-couriers/{type}', 'CourierController@listCouriers')->name('couriers.list-couriers');
Route::post('forward-couriers/{id} ','CourierController@forwardCouriers')->name('couriers.forward-couriers');