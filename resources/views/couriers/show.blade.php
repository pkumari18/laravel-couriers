@extends('layout1')
@section('content')
<style>
.uper {
	margin-top: 40px;
}
</style>
<div class=" uper">
	<div class="body">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br />
		@endif
		<a href="{{route('couriers.index')}}" class="btn btn-primary">Back</a>
		<div class="row">
			<div class="col-sm-8">
				<div class="form-group">
					<div class="col sm-2">
						<h6>Form:{{ $courier->from }}</h6>
					</div>
					<div class="col sm-2">
						<h6>To:{{ $courier->to }}</h6>
					</div>
					<div class="col sm-2">
						<h6>Received By:{{$courier->received_by }}</h6>
					</div>
				 <div class="col sm-2">
						<h6>Posted By:{{ $courier->posted_by }} </h6>
					</div>


					<label for="handover_users">Handovers To</label>
					<select class="form-control" class="handover_to" id="select2">
						@foreach ($users as $row)
						<option value="{{ $row->id }}">{{ $row->name }}</option>
						@endforeach
					</select><br>
					<button id="submitInwardforward" class="btn btn-primary">Forward By</button>
				</div>
			</div>
			<input type="hidden" id="courier_id" value="{{ $courier->id }}">
			<div class="col-lg-4">
				<div class="form-group">
					<h5>Comments</h5>
					<textarea class="form-control" id="comments" rows="3"></textarea>
				</div>
			<center><button type="button" class="btn btn-primary">Submit</button>	</center>
			</div>
				</div>
			</div>
		</div>
		<div class="row">
		<div class="col-sm-8">
   <div class="form-group">
				<h5><b>Handovers</b></h5>

					 <table id="example"  class="table table-striped table-bordered" style="width:100%">

						<thead>
							<tr>
								<th>Name</th>
								<th>Date of Handover</th>
							</tr>
						</thead>
						<tbody>
							@foreach($handovers as $handover)
							<tr>
								<td>{{ \App\User::where('id', $handover->user_id)->pluck('name')->first() }}</td>
								<td>{{ $handover->created_at }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>

		</div>
		@endsection

			@push('scripts')
				<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
				<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
			<script>
				$(document).ready(function() {
					$('#select2').select2();

					$('#submitInwardforward').on('click', function(){
						var user_id =  $('#select2').val();
						var courier_id= $('#courier_id').val();

						$.post( "/forward-couriers/"+courier_id, {'_token': "{{ csrf_token() }}", 'user_id':user_id }, function(data){
							alert(data);
						});
					});
				});

</script>
@endpush
